//
//  Thread.hpp
//  test
//
//  Created by Hébriel ROUSSEAU on 09/02/2019.
//  Copyright © 2019 eyax. All rights reserved.
//

#ifndef Thread_hpp
#define Thread_hpp

#include <thread>
#include <string>
#include <exception>

namespace ey {
    
    class Thread
    {
    public:
        
        template<class T>
        static std::string getName()
        {
            T t;
            return t.name();
        }
        
        virtual std::string name();
        
        void launch();
        
        virtual void run();
        
        std::thread thread;
        
    };
    
}

#endif /* Thread_hpp */
