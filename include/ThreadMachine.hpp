//
//  ThreadMachine.hpp
//  test
//
//  Created by Hébriel ROUSSEAU on 09/02/2019.
//  Copyright © 2019 eyax. All rights reserved.
//

#ifndef ThreadMachine_hpp
#define ThreadMachine_hpp

#include <unordered_map>
#include <memory>
#include "Thread.hpp"

namespace ey {
    
    class ThreadMachine
    {
    public:
        
        template<class T, class... Args>
        void addThread(Args... args)
        {
            m_threads[  Thread::getName<T>()    ] = std::make_unique<T>(args...); //Can overwrite existing thread
        }
        
        template<class T, class... Args>
        void assignToMainThread(Args... args)
        {
            mainThread = std::make_unique<T>(args...);
        }
        
        void run();
        
    private:
        
        std::unique_ptr<Thread> mainThread;
        std::unordered_map<std::string, std::unique_ptr<Thread>> m_threads;
    };
    
}
#endif /* ThreadMachine_hpp */
