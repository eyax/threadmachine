//
//  ThreadMachine.cpp
//  test
//
//  Created by Hébriel ROUSSEAU on 09/02/2019.
//  Copyright © 2019 eyax. All rights reserved.
//

#include "ThreadMachine.hpp"

namespace ey {
    
    void ThreadMachine::run()
    {
        for(const auto& t : m_threads)
        {
            t.second->launch();
        }
        
        mainThread->run();
        
        for(const auto& t : m_threads)
        {
            t.second->thread.join();
        }
    }
    
}
