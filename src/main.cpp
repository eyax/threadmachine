#include <iostream>
#include "ThreadMachine.hpp"

class MainThread : public ey::Thread
{
public:
	
	std::string name() override //Helps for storing and debugging. Do NOT make threads with the same name!
	{
		return "Main Thread";
	}
	
	void run() override //This is gonna get executed on the actual thread
	{
		std::cout << "[Main Thread]" << std::endl;
	}
};

class AnotherThread : public ey::Thread
{
public:
	
	std::string name() override
	{
		return "Another Thread";
	}
	
	void run() override
	{
		std::cout << "[Another Cool Thread!]" << std::endl;
	}
};

int main(int argc, const char * argv[])
{
	ey::ThreadMachine threadMachine;
	
	threadMachine.assignToMainThread<MainThread>();
	threadMachine.addThread<AnotherThread>();
	
	threadMachine.run(); //Launched all threads, will resume when all threads are finished
	
	std::cout << "All threads have finished." << std::endl;
	
	return 0;
}
