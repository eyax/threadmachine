//
//  Thread.cpp
//  test
//
//  Created by Hébriel ROUSSEAU on 09/02/2019.
//  Copyright © 2019 eyax. All rights reserved.
//

#include "Thread.hpp"

namespace ey {
    
    std::string Thread::name()
    {
        return "Invalid Thread";
    }
    
    void Thread::launch()
    {
        thread = std::thread(&Thread::run, this);
    }
    
    void Thread::run()
    {
        throw std::runtime_error("Runtime Error : The program tried to execute an invalid or not-implemented thread. Name of the thread : " + name() + ".");
    }
}
